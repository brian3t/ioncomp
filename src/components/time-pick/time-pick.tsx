import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'time-pick',
  styleUrl: 'time-pick.css',
  shadow: true,
})
export class TimePick {

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }

}
