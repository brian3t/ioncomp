import type { Components, JSX } from "../dist/types/components";

interface TimePick extends Components.TimePick, HTMLElement {}
export const TimePick: {
  prototype: TimePick;
  new (): TimePick;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
