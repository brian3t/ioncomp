/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
import { proxyCustomElement, HTMLElement, h, Host } from '@stencil/core/internal/client';

const timePickCss = ":host{display:block}";

const TimePick$1 = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h(Host, null, h("slot", null)));
  }
  static get style() { return timePickCss; }
}, [1, "time-pick"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["time-pick"];
  components.forEach(tagName => { switch (tagName) {
    case "time-pick":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, TimePick$1);
      }
      break;
  } });
}

const TimePick = TimePick$1;
const defineCustomElement = defineCustomElement$1;

export { TimePick, defineCustomElement };
