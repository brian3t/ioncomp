import type { Components, JSX } from "../dist/types/components";

interface IonDatetimeS extends Components.IonDatetimeS, HTMLElement {}
export const IonDatetimeS: {
  prototype: IonDatetimeS;
  new (): IonDatetimeS;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
